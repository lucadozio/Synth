package com.example.luca.synth;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by luca on 15/01/18.
 */

class CustomViewPager extends ViewPager {

    private boolean enable;

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent (MotionEvent event) {
        if (this.enable)
            return super.onTouchEvent(event);
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent (MotionEvent event) {
        if (this.enable)
            return super.onInterceptTouchEvent(event);
        return false;
    }

    public void setPagingEnable (boolean enable) {
        this.enable = enable;
    }
}
