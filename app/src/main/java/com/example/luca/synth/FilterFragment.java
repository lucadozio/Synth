package com.example.luca.synth;

import android.content.Context;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import it.beppi.knoblibrary.Knob;

/**
 * Created by luca on 15/01/18.
 */

public class FilterFragment extends Fragment {

    private static FilterFragment instance = null;
    private static final String TAG = "Filter";
    private TextView tvKnobDx, tvKnobSx;
    double HIGH, LOW;


    public FilterFragment() {}

    public static synchronized Fragment newInstance() {
        if (instance == null) {
            instance = new FilterFragment();
        }
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "FonResume");
        Knob sx = getView().findViewById(R.id.knob2);
        Knob dx = getView().findViewById(R.id.knob3);

        String actual = Controller.getInstance().getFilter();
        if (actual == null) {
            sx.setState((int) LOW);
        } else if (actual.equals("low")) {
            tvKnobSx.setText(String.valueOf((Controller.getInstance().getLowCutoff())*20));
            sx.setState((int) LOW);
            Log.i(TAG, "dentro low");
        } else if (actual.equals("high")) {
            tvKnobSx.setText(String.valueOf((Controller.getInstance().getHighCutoff())*20));
            sx.setState((int) HIGH);
            Log.i(TAG, "dentro high");
        } else {
            tvKnobSx.setText(String.valueOf((Controller.getInstance().getHighCutoff())*20));
            tvKnobDx.setText(String.valueOf((Controller.getInstance().getLowCutoff())*20));
            sx.setState((int) HIGH);
            dx.setState((int) LOW);
            Log.i(TAG, "dentro band");
        }
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        Log.i(TAG, "FonCreateView");

        final View rootView = inflater.inflate(R.layout.fragment_filter, viewGroup, false);
        LOW = Controller.getInstance().getLowCutoff();
        HIGH = Controller.getInstance().getHighCutoff();

        Log.i(TAG, "LOW = " + LOW + ", HIGH = " + HIGH);
        Log.i(TAG, "Filter type = " + Controller.getInstance().getFilter());

        final RadioGroup filterType = rootView.findViewById(R.id.radioGroup);
        final RadioButton radioLow = filterType.findViewById(R.id.radioButton8);
        final RadioButton radioHigh = filterType.findViewById(R.id.radioButton9);
        final RadioButton radioBand = filterType.findViewById(R.id.radioButton10);

        tvKnobSx = rootView.findViewById(R.id.tvKnobSx);
        tvKnobDx = rootView.findViewById(R.id.tvKnobDx);

        final Knob sx = rootView.findViewById(R.id.knob2);
        sx.setOnStateChanged(new Knob.OnStateChanged() {
            @Override
            public void onState(int state) {
                String s = String.valueOf(state);
                Log.i(TAG, s);
                if (radioLow.isChecked()) {
                    Controller.getInstance().setLowCutoff(state * 20);
                    tvKnobSx.setText(String.valueOf((Controller.getInstance().getLowCutoff())*20));
                } else if (radioHigh.isChecked()) {
                    Controller.getInstance().setHighCutoff(state * 20);
                    tvKnobSx.setText(String.valueOf((Controller.getInstance().getHighCutoff())*20));
                } else {
                    Controller.getInstance().setHighCutoff(state * 20);
                    tvKnobSx.setText(String.valueOf((Controller.getInstance().getHighCutoff())*20));
                }
            }
        });

        final Knob dx = rootView.findViewById(R.id.knob3);
        dx.setOnStateChanged(new Knob.OnStateChanged() {
            @Override
            public void onState(int state) {
                String s = String.valueOf(state);
                Log.i(TAG, "KnobDx: " + s);
                if (radioBand.isChecked()) {
                    Controller.getInstance().setLowCutoff(state * 20);
                    tvKnobDx.setText(String.valueOf((Controller.getInstance().getLowCutoff())*20));
                }
            }
        });

        filterType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == radioBand.getId()) {
                    sx.setEnabled(true);
                    sx.setState(Controller.getInstance().getHighCutoff());
                    dx.setEnabled(true);
                    dx.setState(Controller.getInstance().getLowCutoff());
                    dx.setKnobColor(Color.parseColor("#FF212121"));
                    dx.setKnobCenterColor(Color.parseColor("#FF505050"));
                    dx.setStateMarkersColor(Color.BLACK);
                    Log.d(TAG, "filtro passa banda attivo");
                    Controller.getInstance().setFilter("band");
                } else if (group.getCheckedRadioButtonId() == radioLow.getId()) {
                    sx.setEnabled(true);
                    sx.setState(Controller.getInstance().getLowCutoff());
                    dx.setEnabled(false);
                    dx.setKnobColor(Color.parseColor("#FF444444"));
                    dx.setKnobCenterColor(Color.parseColor("#FF666666"));
                    dx.setStateMarkersColor(Color.parseColor("#FF666666"));
                    Log.d(TAG, "filtro passa basso attivo");
                    Controller.getInstance().setHighCutoff(0);
                    Controller.getInstance().setFilter("low");
                } else {
                    sx.setEnabled(true);
                    sx.setState(Controller.getInstance().getHighCutoff());
                    dx.setEnabled(false);
                    dx.setKnobColor(Color.parseColor("#FF444444"));
                    dx.setKnobCenterColor(Color.parseColor("#FF666666"));
                    dx.setStateMarkersColor(Color.parseColor("#FF666666"));
                    Log.d(TAG, "filtro passa alto attivo");
                    Controller.getInstance().setLowCutoff(20000);
                    Controller.getInstance().setFilter("high");
                }
            }
        });

        Switch enable = rootView.findViewById(R.id.switch2);
        enable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sx.setEnabled(true);
                    sx.setState(Controller.getInstance().getLowCutoff());
                    sx.setKnobColor(Color.parseColor("#FF212121"));
                    sx.setKnobCenterColor(Color.parseColor("#FF505050"));
                    sx.setStateMarkersColor(Color.BLACK);
                    dx.setEnabled(false);
                    radioLow.setEnabled(true);
                    radioLow.setSelected(true);
                    radioHigh.setEnabled(true);
                    radioBand.setEnabled(true);
                    Controller.getInstance().filterActive = true;
                    Controller.getInstance().setFilter("low");
                } else {
                    Controller.getInstance().setLowCutoff(20000);
                    Controller.getInstance().setHighCutoff(0);
                    sx.setEnabled(false);
                    sx.setKnobColor(Color.parseColor("#FF444444"));
                    sx.setKnobCenterColor(Color.parseColor("#FF666666"));
                    sx.setStateMarkersColor(Color.parseColor("#FF666666"));
                    dx.setEnabled(false);
                    dx.setKnobColor(Color.parseColor("#FF444444"));
                    dx.setKnobCenterColor(Color.parseColor("#FF666666"));
                    dx.setStateMarkersColor(Color.parseColor("#FF666666"));
                    radioLow.setEnabled(false);
                    radioHigh.setEnabled(false);
                    radioBand.setEnabled(false);
                    Controller.getInstance().filterActive = false;
                    Controller.getInstance().setFilter(null);
                }
            }
        });

        return rootView;
    }
}