package com.example.luca.synth;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.beppi.knoblibrary.Knob;

/**
 * Created by luca on 15/01/18.
 */

public class DelayFragment extends Fragment {

    private static DelayFragment instance = null;
    private static final String TAG = "DelayFragment";
    private static TextView tvTime, tvDryWet;

    public DelayFragment() {}

    public static synchronized Fragment newInstance() {
        if (instance == null) {
            instance = new DelayFragment();
        }
        return instance;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "onResume DelayFragment");
        Knob knob4 = getView().findViewById(R.id.knob4);
        knob4.setState(Controller.getInstance().getDelayTime());
        double d = Controller.getInstance().getDelayTime()*10000/30;
        if (d<1000) {
            double r = d%10;
            tvTime.setText("0," + String.valueOf((int) r));
        } else if (d > 999 && d < 2000) {
            double r = d%10;
            tvTime.setText("1," + String.valueOf((int) r));
        } else if (d > 1999 && d < 3000) {
            double r = d%10;
            tvTime.setText("2," + String.valueOf((int) r));
        } else if (d > 2999 && d < 4000) {
            double r = d%10;
            tvTime.setText("3," + String.valueOf((int) r));
        } else if (d > 3999 && d < 5000) {
            double r = d%10;
            tvTime.setText("4," + String.valueOf((int) r));
        } else if (d > 4999 && d < 6000) {
            double r = d%10;
            tvTime.setText("5," + String.valueOf((int) r));
        } else if (d > 5999 && d < 7000) {
            double r = d%10;
            tvTime.setText("6," + String.valueOf((int) r));
        } else if (d > 6999 && d < 8000) {
            double r = d%10;
            tvTime.setText("7," + String.valueOf((int) r));
        } else if (d > 7999 && d < 9000) {
            double r = d%10;
            tvTime.setText("8," + String.valueOf((int) r));
        } else if (d > 8999 && d < 10000) {
            double r = d%10;
            tvTime.setText("9," + String.valueOf((int) r));
        } else {
            tvTime.setText("10.0");
        }
        Knob knob5 = getView().findViewById(R.id.knob5);
        knob5.setState(Controller.getInstance().getDryWet());
        tvDryWet.setText(String.valueOf((Controller.getInstance().getDryWet())));
        Log.i(TAG, String.valueOf(Controller.getInstance().getDelayTime()));
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_delay, viewGroup, false);

        double DELAY_LEVEL = Controller.getInstance().DELAY_TIME;
        Log.i(TAG, "Inizio " + DELAY_LEVEL);

        final Knob knob5 = rootView.findViewById(R.id.knob5);

        tvTime = rootView.findViewById(R.id.tvTime);
        tvDryWet = rootView.findViewById(R.id.tvDryWet);

        Knob knob4 = rootView.findViewById(R.id.knob4);
        knob4.setDefaultState(Controller.getInstance().getDelayTime());
        knob4.setOnStateChanged(new Knob.OnStateChanged() {
            @Override
            public void onState(int state) {
                String s = String.valueOf(state);
                Log.i(TAG, s);
                Controller.getInstance().setDelayTime(state);
                if (state != 0) {
                    knob5.setEnabled(true);
                    knob5.setKnobCenterColor(Color.parseColor("#FF505050"));
                    knob5.setKnobColor(Color.parseColor("#FF212121"));
                } else {
                    knob5.setEnabled(false);
                    knob5.setKnobCenterColor(Color.parseColor("#FF666666"));
                    knob5.setKnobColor(Color.parseColor("#FF444444"));
                    Controller.getInstance().setDryWet(0);
                }
                double d = state*10000/30;
                if (d<1000) {
                    double r = d%10;
                    tvTime.setText("0," + String.valueOf((int) r));
                } else if (d > 999 && d < 2000) {
                    double r = d%10;
                    tvTime.setText("1," + String.valueOf((int) r));
                } else if (d > 1999 && d < 3000) {
                    double r = d%10;
                    tvTime.setText("2," + String.valueOf((int) r));
                } else if (d > 2999 && d < 4000) {
                    double r = d%10;
                    tvTime.setText("3," + String.valueOf((int) r));
                } else if (d > 3999 && d < 5000) {
                    double r = d%10;
                    tvTime.setText("4," + String.valueOf((int) r));
                } else if (d > 4999 && d < 6000) {
                    double r = d%10;
                    tvTime.setText("5," + String.valueOf((int) r));
                } else if (d > 5999 && d < 7000) {
                    double r = d%10;
                    tvTime.setText("6," + String.valueOf((int) r));
                } else if (d > 6999 && d < 8000) {
                    double r = d%10;
                    tvTime.setText("7," + String.valueOf((int) r));
                } else if (d > 7999 && d < 9000) {
                    double r = d%10;
                    tvTime.setText("8," + String.valueOf((int) r));
                } else if (d > 8999 && d < 10000) {
                    double r = d%10;
                    tvTime.setText("9," + String.valueOf((int) r));
                } else {
                    tvTime.setText("10.0");
                }
                Log.i(TAG, String.valueOf(d%1000));
            }
        });

        knob5.setOnStateChanged(new Knob.OnStateChanged() {
            @Override
            public void onState(int state) {
                String s = String.valueOf(state);
                Log.i(TAG, s);
                Controller.getInstance().setDryWet(state);
                tvDryWet.setText(String.valueOf(state));
            }
        });

        Log.i(TAG, "Fine " + DELAY_LEVEL);

        return rootView;
    }
}

