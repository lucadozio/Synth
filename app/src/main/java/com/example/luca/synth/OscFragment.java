package com.example.luca.synth;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import it.beppi.knoblibrary.Knob;

/**
 * Created by luca on 15/01/18.
 */

public class OscFragment extends Fragment {

    private static OscFragment instance = null;
    private final static String TAG = "OscFragment";
    private static TextView tvOsc1, tvOsc2;
    private int amplitude = 15;

    public OscFragment() {}

    public static synchronized Fragment newInstance() {
        if (instance == null) {
            instance = new OscFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        final View rootView = inflater.inflate(R.layout.fragment_osc, viewGroup, false);
        amplitude = Controller.getInstance().getKnobVolume();
        int secondAmplitude = Controller.getInstance().getSecondKnobVolume();

        RadioGroup osc1Type = rootView.findViewById(R.id.radioGroup1);
        final RadioButton sine1 = osc1Type.findViewById(R.id.radioSine1);
        final RadioButton square1 = osc1Type.findViewById(R.id.radioSquare1);
        final RadioButton sawtooth1 = osc1Type.findViewById(R.id.radioSawtooth1);

        final RadioGroup osc2Synth = rootView.findViewById(R.id.radioGroup2);
        final RadioButton add = osc2Synth.findViewById(R.id.additive);
        final RadioButton sub = osc2Synth.findViewById(R.id.subtractive);

        RadioGroup osc2Type = rootView.findViewById(R.id.radioGroup3);
        final RadioButton sine2 = osc2Type.findViewById(R.id.radioSine2);
        final RadioButton square2 = osc2Type.findViewById(R.id.radioSquare2);
        final RadioButton sawtooth2 = osc2Type.findViewById(R.id.radioSawtooth2);

        final Knob volume2 = rootView.findViewById(R.id.knob1);

        tvOsc1 = rootView.findViewById(R.id.tvOsc1);
        tvOsc2 = rootView.findViewById(R.id.tvOsc2);

        Controller.getInstance().setOscillator(0,"sine");
        Controller.getInstance().setOscillator(1,"sine");

        osc1Type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == sine1.getId()) {
                    Log.i(TAG, "Radiogroup1. Wave form: Sine");
                    Controller.getInstance().setOscillator(0, "sine");
                } else if (group.getCheckedRadioButtonId() == square1.getId()) {
                    Log.i(TAG, "Radiogroup1. Wave form: Square");
                    Controller.getInstance().setOscillator(0, "square");
                } else if (group.getCheckedRadioButtonId() == sawtooth1.getId()){
                    Log.i(TAG,"Radiogroup1. Wave form: Sawtooth");
                    Controller.getInstance().setOscillator(0, "sawtooth");
                }
            }
        });

        osc2Synth.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == add.getId()) {
                    Log.i(TAG, "Radiogroup2. Synthesis: Additive");
                    Controller.getInstance().unsetSynthesis();
                    Controller.getInstance().setAdditiveLink();
                } else if (group.getCheckedRadioButtonId() == sub.getId()) {
                    Log.i(TAG, "Radiogroup2. Synthesis: subtractive");
                    Controller.getInstance().unsetSynthesis();
                    Controller.getInstance().setSubtractiveLink();
                }
            }
        });

        osc2Type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == sine2.getId()) {
                    Log.i(TAG, "Radiogroup3. Wave form: Sine");
                    Controller.getInstance().setOscillator(1, "sine");
                } else if (group.getCheckedRadioButtonId() == square2.getId()) {
                    Log.i(TAG, "Radiogroup3. Wave form: Square");
                    Controller.getInstance().setOscillator(1, "square");
                } else if (group.getCheckedRadioButtonId() == sawtooth2.getId()){
                    Log.i(TAG,"Radiogroup3. Wave form: Sawtooth");
                    Controller.getInstance().setOscillator(1, "sawtooth");
                }
            }
        });

        Switch enable = rootView.findViewById(R.id.enableOsc);
        enable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rootView.findViewById(R.id.additive).setEnabled(true);
                    rootView.findViewById(R.id.subtractive).setEnabled(true);
                    rootView.findViewById(R.id.radioSine2).setEnabled(true);
                    rootView.findViewById(R.id.radioSquare2).setEnabled(true);
                    rootView.findViewById(R.id.radioSawtooth2).setEnabled(true);
                    TextView A = rootView.findViewById(R.id.textView20);
                    A.setTextColor(Color.parseColor("#000000"));
                    TextView B = rootView.findViewById(R.id.textView3);
                    B.setTextColor(Color.parseColor("#000000"));
                    volume2.setEnabled(true);
                    volume2.setKnobCenterColor(Color.parseColor("#FF505050"));
                    volume2.setKnobColor(Color.parseColor("#FF212121"));
                    Controller.getInstance().twoOscillators(true);
                    Controller.getInstance().setAdditiveLink();
                } else {
                    rootView.findViewById(R.id.additive).setEnabled(false);
                    rootView.findViewById(R.id.subtractive).setEnabled(false);
                    rootView.findViewById(R.id.radioSine2).setEnabled(false);
                    rootView.findViewById(R.id.radioSquare2).setEnabled(false);
                    rootView.findViewById(R.id.radioSawtooth2).setEnabled(false);
                    TextView A = rootView.findViewById(R.id.textView20);
                    A.setTextColor(Color.parseColor("#FF666666"));
                    TextView B = rootView.findViewById(R.id.textView3);
                    B.setTextColor(Color.parseColor("#FF666666"));
                    volume2.setEnabled(false);
                    volume2.setKnobCenterColor(Color.parseColor("#FF666666"));
                    volume2.setKnobColor(Color.parseColor("#FF444444"));
                    Controller.getInstance().twoOscillators(false);
                    Controller.getInstance().unsetSynthesis();
                }
            }
        });

        Knob knob = rootView.findViewById(R.id.knob);
        knob.setOnStateChanged(new Knob.OnStateChanged() {
            @Override
            public void onState(int state) {
                String s = String.valueOf(state);
                Log.i(TAG, s);
                Controller.getInstance().setAmplitude(state);
                Controller.getInstance().setKnobVolume(state);
                tvOsc1.setText(String.valueOf(state));
            }
        });

        Knob knob1 = rootView.findViewById(R.id.knob1);
        knob1.setOnStateChanged(new Knob.OnStateChanged() {
            @Override
            public void onState(int state) {
                String s = String.valueOf(state);
                Log.i(TAG, s);
                Controller.getInstance().setSecondAmplitude(state);
                Controller.getInstance().setSecondKnobVolume(state);
                tvOsc2.setText(String.valueOf(state));
            }
        });

        knob.setState(amplitude);
        knob1.setState(secondAmplitude);
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "onResume");

        Controller.getInstance().getAmplitude();
        tvOsc1.setText(String.valueOf(Controller.getInstance().getKnobVolume()));

        Knob knob = getView().findViewById(R.id.knob);
        knob.setDefaultState(Controller.getInstance().getKnobVolume());

        Knob knob1 = getView().findViewById(R.id.knob1);
        knob1.setDefaultState(Controller.getInstance().getSecondKnobVolume());
    }
}
