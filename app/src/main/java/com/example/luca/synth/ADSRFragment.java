package com.example.luca.synth;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

import java.util.ArrayList;

/**
 * Created by luca on 15/01/18.
 */

public class ADSRFragment extends Fragment {

    private static ADSRFragment instance = null;
    private static final String TAG = "ADSR fragment";

    public ADSRFragment() {}

    public static synchronized Fragment newInstance() {
        if (instance == null) {
            instance = new ADSRFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_adsr, viewGroup, false);

        ArrayList<Double> values = Controller.getInstance().getADSR();

        VerticalSeekBar attack = rootView.findViewById(R.id.mySeekBar_a);
        attack.setProgress(1);
        attack.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = ((double) progress / 10.0);
                Controller.getInstance().setADSR(0, value);
                Log.i(TAG,"new attack value " + value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        VerticalSeekBar decay = rootView.findViewById(R.id.mySeekBar_d);
        decay.setProgress(1);
        decay.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = ((double) progress / 10.0);
                Controller.getInstance().setADSR(1, value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        VerticalSeekBar sustain = rootView.findViewById(R.id.mySeekBar_s);
        sustain.setProgress(2);
        sustain.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = ((double) progress / 10.0);
                Controller.getInstance().setADSR(2, value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        VerticalSeekBar release = rootView.findViewById(R.id.mySeekBar_r);
        release.setProgress(1);
        release.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = ((double) progress / 10.0);
                Controller.getInstance().setADSR(3, value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return rootView;
    }

}

