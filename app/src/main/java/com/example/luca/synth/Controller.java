package com.example.luca.synth;

import android.util.Log;

import com.jsyn.JSyn;
import com.jsyn.Synthesizer;
import com.jsyn.instruments.DualOscillatorSynthVoice;
import com.jsyn.unitgen.Add;
import com.jsyn.unitgen.EnvelopeDAHDSR;
import com.jsyn.unitgen.FilterHighPass;
import com.jsyn.unitgen.FilterLowPass;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SquareOscillator;
import com.jsyn.unitgen.Subtract;
import com.jsyn.unitgen.UnitVoice;
import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;

import java.util.ArrayList;

/**
 * Created by andrea on 15/01/18.
 */

public class Controller {

    private static Controller ourInstance = null;
    private final Synthesizer synthesizer = JSyn.createSynthesizer(new JSynAndroidAudioDevice());
    private UnitVoice[] voices, secondVoices, delayVoices, delaySecondVoices;
    private VoiceAllocator allocator, delayAllocator, secondAllocator, secondDelayAllocator;
    private SquareOscillator trigger = new SquareOscillator();
    private EnvelopeDAHDSR ADSR = new EnvelopeDAHDSR();
    private Add additiveUnit = new Add();
    private Subtract subtractiveUnit = new Subtract();
    private FilterLowPass lowPass = new FilterLowPass();
    private FilterHighPass highPass = new FilterHighPass();
    private LineOut lineOut = new LineOut();
    private ArrayList<Double> pitches = new ArrayList<>();
    private int currentMaxOctave = 4, knobVolume = 15, secondKnobVolume = 15, INT_DELAY_TIME = 0, dryWet = 20, highCutoff = 0;
    private double amplitude = 15, secondAmplitude = 15, lowCutoff = 20000;
    static double DELAY_TIME = 0.0;
    private static final double fref = 440.0;
    private boolean hold = false, twoOsc = false;
    public boolean filterActive = false;
    private String filter = null;
    private static final String TAG = "Controller activity";

    private Controller() {
        loadPitches();
        setupADSR();
        setupSynth();
    }

    public static synchronized Controller getInstance() {
        if (ourInstance == null) {
            ourInstance = new Controller();
        }
        return ourInstance;
    }

    private void setupSynth() {
        Log.d(TAG, "setup synth");
        synthesizer.add(trigger);
        synthesizer.add(ADSR);
        synthesizer.add(additiveUnit);
        synthesizer.add(subtractiveUnit);
        synthesizer.add(lowPass);
        synthesizer.add(highPass);
        synthesizer.add(lineOut);

        trigger.output.connect(ADSR.input);

        lowPass.frequency.set(lowCutoff);
        highPass.frequency.set(highCutoff);

        voices = new UnitVoice[5];
        secondVoices = new UnitVoice[5];
        delayVoices = new UnitVoice[5];
        delaySecondVoices = new UnitVoice[5];

        ADSR.attack.set(1.0);

        for (int i=0; i<5; i++) {

            DualOscillatorSynthVoice voice = new DualOscillatorSynthVoice();
            synthesizer.add(voice);
            voice.usePreset(3);
            // Togliere il commento per abilitare l'ADSR
            //ADSR.output.connect(voice.amplitude);
            voice.getOutput().connect(highPass.input);

            DualOscillatorSynthVoice delayVoice = new DualOscillatorSynthVoice();
            synthesizer.add(delayVoice);
            delayVoice.usePreset(3);
            delayVoice.getOutput().connect(highPass.input);

            DualOscillatorSynthVoice secondVoice = new DualOscillatorSynthVoice();
            synthesizer.add(secondVoice);
            secondVoice.usePreset(3);
            // Togliere il commento per abilitare l'ADSR
            //ADSR.output.connect(secondVoice.amplitude);

            DualOscillatorSynthVoice delaySecondVoice = new DualOscillatorSynthVoice();
            synthesizer.add(delaySecondVoice);
            delaySecondVoice.usePreset(3);

            voices[i] = voice;
            delayVoices[i] = delayVoice;
            secondVoices[i] = secondVoice;
            delaySecondVoices[i] = delaySecondVoice;

        }

        highPass.output.connect(lowPass.input);
        lowPass.output.connect(0, lineOut.input, 0);
        lowPass.output.connect(0, lineOut.input, 1);

        allocator = new VoiceAllocator(voices);
        delayAllocator = new VoiceAllocator(delayVoices);
        secondAllocator = new VoiceAllocator(secondVoices);
        secondDelayAllocator = new VoiceAllocator(delaySecondVoices);

        synthesizer.start();
        lineOut.start();
    }

    public void setupADSR() {
        ADSR.attack.set(0.5);
        ADSR.decay.set(1.0);
        ADSR.sustain.set(amplitude);
        ADSR.release.set(0.7);
    }

    public void setOscillator(int osc, String type) {
        if (osc == 0) {
            switch (type.toLowerCase()) {
                case "sine":
                    for (int i = 0; i < 5; i++) {
                        voices[i].usePreset(3);
                        delayVoices[i].usePreset(3);
                    }
                    break;
                case "square":
                    for (int i = 0; i < 5; i++) {
                        voices[i].usePreset(5);
                        delayVoices[i].usePreset(5);
                    }
                    break;
                case "sawtooth":
                    for (int i = 0; i < 5; i++) {
                        voices[i].usePreset(2);
                        delayVoices[i].usePreset(2);
                    }
                    break;
            }
        } else {
            switch (type.toLowerCase()) {
                case "sine":
                    for (int i = 0; i < 5; i++) {
                        secondVoices[i].usePreset(3);
                        delaySecondVoices[i].usePreset(3);
                    }
                    break;
                case "square":
                    for (int i = 0; i < 5; i++) {
                        secondVoices[i].usePreset(5);
                        delaySecondVoices[i].usePreset(5);
                    }
                    break;
                case "sawtooth":
                    for (int i = 0; i < 5; i++) {
                        secondVoices[i].usePreset(2);
                        delaySecondVoices[i].usePreset(2);
                    }
                    break;
            }
        }
    }

    public void keyPressed(final int position) {

        if (filterActive == false) {
            lowCutoff = 20000;
            highCutoff = 0;
        }

        Log.d(TAG, "playing " + pitches.get(position));

        final double frequency = pitches.get(position);
        final TimeStamp timeStamp = new TimeStamp(synthesizer.getCurrentTime());
        double dryAmplitude = amplitude*dryWet/20;
        final double wetAmplitude = amplitude - dryAmplitude;
        double secondDryAmplitude = secondAmplitude*dryAmplitude/20;
        final double secondWetAmplitude = secondAmplitude - secondDryAmplitude;

        allocator.noteOn(position, frequency, dryAmplitude, timeStamp);
        if (twoOsc)
            secondAllocator.noteOn(position, frequency, currentSecondAmplitude(), timeStamp);
        Log.i(TAG, "SecondAmplitude = " + secondAmplitude + ", lowcutoff = " + lowCutoff);
        Log.i(TAG, "amplitude = " + amplitude + ", lowcutoff = " + lowCutoff);

        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep((long) DELAY_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                double amp = 0.7;
                double ampDelay = wetAmplitude * amp;
                double amps = 0.7;
                double ampSecondDelay = secondWetAmplitude * amps;

                for (int i=0; i<5; i++) {
                    TimeStamp timeStamp = new TimeStamp(synthesizer.getCurrentTime());
                    delayAllocator.noteOn(position, frequency, ampDelay, timeStamp);
                    if (twoOsc)
                        secondDelayAllocator.noteOn(position, frequency, ampSecondDelay, timeStamp);
                    try {
                        Thread.sleep((long) DELAY_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    delayAllocator.noteOff(position, new TimeStamp(synthesizer.getCurrentTime()+(DELAY_TIME)));
                    if (twoOsc)
                        secondDelayAllocator.noteOff(position, new TimeStamp(synthesizer.getCurrentTime()+DELAY_TIME));
                    ampDelay = ampDelay*amp;
                    ampSecondDelay = ampSecondDelay*amps;
                    Log.i(TAG, "Delay repetition index = " + i);
                }
                delayAllocator.noteOff(position, new TimeStamp(synthesizer.getCurrentTime()));
                if (twoOsc)
                    secondDelayAllocator.noteOff(position, new TimeStamp(synthesizer.getCurrentTime()));
            }
        }).start();
    }

    public void keyReleased(int position) {
        Log.d(TAG, "stop playing " + pitches.get(position));
        TimeStamp timeStamp = new TimeStamp(synthesizer.getCurrentTime());
        allocator.noteOff(position, timeStamp);
        if (twoOsc)
            secondAllocator.noteOff(position, timeStamp);
    }

    private void loadPitches() {
        for (int i=21; i>0; i--) {
            pitches.add(fref * offset(-i));
        }
        pitches.add(fref);
        for (int i=1; i<4; i++)
            pitches.add(fref * offset(i));
    }

    private double offset(int n) {
        return Math.pow(2, (double) n/12);
    }

    public void octaveChange(int d) {
        if (d > 0) {
            if (currentMaxOctave < 8) {
                for (int i = 0; i < 25; i++)
                    pitches.set(i, pitches.get(i) * 2);
                currentMaxOctave++;
            }
        }
        else {
            if (currentMaxOctave > 2) {
                for (int i = 0; i < 25; i++)
                    pitches.set(i, pitches.get(i) / 2);
                currentMaxOctave--;
            }
        }
    }

    public int getCurrentMaxOctave() {
        return currentMaxOctave;
    }

    public void setHoldMode() {
        hold = !hold;
        if (hold == false) {
            TimeStamp timeStamp = new TimeStamp(synthesizer.getCurrentTime());
            allocator.allNotesOff(timeStamp);
            secondAllocator.allNotesOff(timeStamp);
        }
    }

    public boolean hold() {
        return hold;
    }

    public void setADSR(int index, double value) {
        switch (index) {
            case 0:
                ADSR.attack.set(value);
                return;
            case 1:
                ADSR.decay.set(value);
                return;
            case 2:
                ADSR.sustain.set(value);
                return;
            case 3:
                ADSR.release.set(value);
                return;
        }
    }

    public int getDelayTime() {
        return INT_DELAY_TIME;
    }

    public void setDelayTime(int d) {
        INT_DELAY_TIME = d;
        int maxD = 10000; // max delay millisecond
        int kl = 30; // knob line
        DELAY_TIME = d*maxD/kl;
    }

    public int getDryWet() {
        return 20-dryWet;
    }

    public void setDryWet(int dryWet) {
        this.dryWet = 20-dryWet;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(int a) {
        this.amplitude = a;
        int middleAmp = 15; // middle amplitude
        int kl = 30; // knob line
        amplitude = a*middleAmp/30;
    }

    public int getKnobVolume() {
        return knobVolume;
    }

    public void setKnobVolume(int volume) {
        this.knobVolume = volume;
        Log.i(TAG, "setKnobVolume: " + String.valueOf(knobVolume));
    }

    public double currentSecondAmplitude() {
        return secondAmplitude*dryWet/20;
    }

    public void setSecondAmplitude(int a) {
        this.secondAmplitude = a;
        int middleAmp = 15; // middle amplitude millisecond
        int kl = 30; // knob line
        secondAmplitude = a*middleAmp/kl;
    }

    public int getSecondKnobVolume() {
        return secondKnobVolume;
    }

    public void setSecondKnobVolume(int volume) {
        this.secondKnobVolume = volume;
        Log.i(TAG, "setKnobVolume: " + String.valueOf(secondKnobVolume));
    }

    public ArrayList<Double> getADSR() {
        ArrayList<Double> values = new ArrayList<>();
        values.add(ADSR.attack.get());
        values.add(ADSR.decay.get());
        values.add(ADSR.sustain.get());
        values.add(ADSR.release.get());
        return values;
    }

    public int getLowCutoff() {
        return (int)(lowCutoff /20);
    }

    public void setLowCutoff(double lowCutoff) {
        this.lowCutoff = lowCutoff;
        lowPass.frequency.set(lowCutoff);
    }

    public int getHighCutoff() {
        return highCutoff/20;
    }

    public void setHighCutoff(int highCutoff) {
        this.highCutoff = highCutoff;
        highPass.frequency.set(highCutoff);
    }

    public void setAdditiveLink() {
        for (int i=0; i<5; i++) {
            voices[i].getOutput().disconnect(highPass.input);
            voices[i].getOutput().connect(additiveUnit.inputA);
            delayVoices[i].getOutput().disconnect(highPass.input);
            delayVoices[i].getOutput().connect(additiveUnit.inputA);

            secondVoices[i].getOutput().connect(additiveUnit.inputB);
            delaySecondVoices[i].getOutput().connect(additiveUnit.inputB);

            additiveUnit.output.connect(highPass.input);
        }
    }

    public void setSubtractiveLink() {
        for (int i=0; i<5; i++) {
            voices[i].getOutput().disconnect(highPass.input);
            voices[i].getOutput().connect(subtractiveUnit.inputA);
            delayVoices[i].getOutput().disconnect(highPass.input);
            delayVoices[i].getOutput().connect(subtractiveUnit.inputA);

            secondVoices[i].getOutput().connect(subtractiveUnit.inputB);
            delaySecondVoices[i].getOutput().connect(subtractiveUnit.inputB);

            subtractiveUnit.output.connect(highPass.input);
        }
    }

    public void unsetSynthesis() {
        for (int i=0; i<5; i++) {
            voices[i].getOutput().disconnect(additiveUnit.inputA);
            voices[i].getOutput().disconnect(subtractiveUnit.inputA);
            voices[i].getOutput().connect(highPass.input);

            delayVoices[i].getOutput().disconnect(additiveUnit.inputA);
            delayVoices[i].getOutput().disconnect(subtractiveUnit.inputA);
            delayVoices[i].getOutput().connect(highPass.input);

            secondVoices[i].getOutput().disconnect(subtractiveUnit.inputB);
            secondVoices[i].getOutput().disconnect(additiveUnit.inputB);
            delaySecondVoices[i].getOutput().disconnect(subtractiveUnit.inputB);
            delaySecondVoices[i].getOutput().disconnect(additiveUnit.inputB);

            subtractiveUnit.output.disconnect(highPass.input);
            additiveUnit.output.disconnect(highPass.input);
        }
    }

    public void twoOscillators(boolean b) {
        twoOsc = b;
    }

    public void setFilter(String s) {
        filter = s;
    }

    public String getFilter() {
        return filter;
    }

}
