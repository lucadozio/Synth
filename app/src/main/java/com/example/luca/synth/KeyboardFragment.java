package com.example.luca.synth;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.awt.font.TextAttribute;

/**
 * Created by andrea on 15/01/18.
 */

public class KeyboardFragment extends Fragment {

    private static KeyboardFragment instance = null;
    private static final String TAG = "Keyboard fragment";

    public KeyboardFragment() {}

    public static synchronized Fragment newInstance() {
        if (instance == null) {
            instance = new KeyboardFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_keyboard, viewGroup, false);

        final TextView octaves = rootView.findViewById(R.id.tvCurrentOctav);

        final Piano piano = rootView.findViewById(R.id.piano);
        piano.setPianoKeyListener(new Piano.PianoKeyListener() {
            @Override
            public void keyPressed(int id, int action) {
                if (action == 0) {
                    Log.d(TAG, id + " pressed");
                    Controller.getInstance().keyPressed(id);
                }
                else {
                    if (!Controller.getInstance().hold()) {
                        Log.d(TAG, id + " released");
                        Controller.getInstance().keyReleased(id);
                    }
                }
            }
        });

        final Button plus = rootView.findViewById(R.id.octaveUp);
        final Button minus = rootView.findViewById(R.id.octaveDown);

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Controller.getInstance().octaveChange(1);
                octaves.setText(String.valueOf((Controller.getInstance().getCurrentMaxOctave())-1)+"-"+String.valueOf(Controller.getInstance().getCurrentMaxOctave()));
                if (Controller.getInstance().getCurrentMaxOctave() == 8)
                    plus.setEnabled(false);
                if (!minus.isEnabled())
                    minus.setEnabled(true);
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Controller.getInstance().octaveChange(-1);
                octaves.setText(String.valueOf((Controller.getInstance().getCurrentMaxOctave())-1)+"-"+String.valueOf(Controller.getInstance().getCurrentMaxOctave()));
                if (Controller.getInstance().getCurrentMaxOctave() == 2)
                    minus.setEnabled(false);
                if (!plus.isEnabled())
                    plus.setEnabled(true);
            }
        });

        final Button hold = rootView.findViewById(R.id.buttonHold);
        hold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Controller.getInstance().setHoldMode();
                if (Controller.getInstance().hold() == true) {
                    hold.setBackgroundColor(Color.parseColor("#FF212121"));
                    hold.setTextColor(Color.parseColor("#FFFFFFFF"));
                } else {
                    hold.setBackgroundColor(Color.LTGRAY);
                    hold.setTextColor(Color.BLACK);
                    //piano.deleteFingers();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume PianoFragment");
    }
}